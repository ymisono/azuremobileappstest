namespace BookReviewAppServer.Migrations
{
    using DataObjects;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BookReviewAppServer.Models.MobileServiceContext>
    {
        public Configuration()
        {
            //AutomaticMigrationsEnabled = false;

            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(BookReviewAppServer.Models.MobileServiceContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //context.ReviewEntries.AddOrUpdate(
            //     new ReviewEntry { BookTitle = "�ۃn�Q", Deleted = false, Rate = 1, Version = Version. }
                 //new ReviewEntry { BookTitle = "���i�S�̉���", Deleted = false },
                 //new ReviewEntry { BookTitle = "�g���E", Deleted = false }
                //);
        }
    }
}
