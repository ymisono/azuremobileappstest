﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(BookReviewAppServer.Startup))]

namespace BookReviewAppServer
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureMobileApp(app);
        }
    }
}