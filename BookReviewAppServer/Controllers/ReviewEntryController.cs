﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using BookReviewAppServer.DataObjects;
using BookReviewAppServer.Models;

namespace BookReviewAppServer.Controllers
{
    [Authorize]
    public class ReviewEntryController : TableController<ReviewEntry>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            MobileServiceContext context = new MobileServiceContext();
            DomainManager = new EntityDomainManager<ReviewEntry>(context, Request);
        }

        // GET tables/ReviewEntry
        public IQueryable<ReviewEntry> GetAllReviewEntry()
        {
            return Query(); 
        }

        // GET tables/ReviewEntry/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<ReviewEntry> GetReviewEntry(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/ReviewEntry/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<ReviewEntry> PatchReviewEntry(string id, Delta<ReviewEntry> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/ReviewEntry
        public async Task<IHttpActionResult> PostReviewEntry(ReviewEntry item)
        {
            ReviewEntry current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/ReviewEntry/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteReviewEntry(string id)
        {
             return DeleteAsync(id);
        }
    }
}
