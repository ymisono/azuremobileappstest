﻿using Microsoft.Azure.Mobile.Server;

namespace BookReviewAppServer.DataObjects
{
    public class ReviewEntry : EntityData //EntityDataはDTOにつけるのが本来だそうだが、簡易的に普通のテーブル扱いにもできるっぽい？
    {
        /// <summary>
        /// 書名
        /// </summary>
        public string BookTitle { get; set; }
        /// <summary>
        /// 評価
        /// </summary>
        public int Rate { get; set; }
        /// <summary>
        /// レビュー文
        /// </summary>
        public string ReviewText { get; set; }

    }
}