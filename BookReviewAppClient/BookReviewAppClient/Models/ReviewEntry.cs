﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookReviewAppClient.Models
{
    public class ReviewEntry
    {
        public string Id { get; set; }

        /// <summary>
        /// 書名
        /// </summary>
        public string BookTitle { get; set; }
        /// <summary>
        /// 評価
        /// </summary>
        public int Rate { get; set; }
        /// <summary>
        /// レビュー文
        /// </summary>
        public string ReviewText { get; set; }
    }
}
