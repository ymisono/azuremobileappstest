﻿using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using BookReviewAppClient.Models;
using System.Threading.Tasks;

namespace BookReviewAppClient
{
    public interface IAuthenticate
    {
        Task<bool> Authenticate();
    };

    public class App : Application
    {
        static internal MobileServiceClient m_client = new MobileServiceClient("http://ym-test-mobile.azurewebsites.net");
        static public MobileServiceClient Client
        {
            get { return m_client; }
        }
        static internal IMobileServiceTable<Models.ReviewEntry> reviewTable;

        //ユーザー
        static internal MobileServiceUser user = null;

        public static IAuthenticate Authenticator { get; private set; }

        public static void Init(IAuthenticate authenticator)
        {
            Authenticator = authenticator;
        }

        public App()
        {
            // The root page of your application
            MainPage = new NavigationPage(new MainPage());
            //MainPage = new TestTabbedPage();
        }


        protected override void OnStart()
        {
            // Handle when your app starts
            reviewTable = m_client.GetTable<ReviewEntry>();

            var review = new ReviewEntry();
            review.BookTitle = "いろはにほへと";
            try
            {
                //reviewTable.InsertAsync(review);
            }
            catch(Exception e)
            {
                var test = e.Message;
            }

        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
