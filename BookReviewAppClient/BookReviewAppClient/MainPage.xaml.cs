﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using BookReviewAppClient.Models;
using Microsoft.WindowsAzure.MobileServices;
using System.Diagnostics;

namespace BookReviewAppClient
{
    public partial class MainPage : ContentPage
    {
        private TestPageViewModel _vm;

        private bool m_authenticated = false;

        public List<ReviewEntry> Reviews { get; set; }

        public MainPage()
        {
            InitializeComponent();
            this.Title = "TestPage";
        }

        async void OnClicked(object sender, EventArgs e)
        {
            //var reviewList = new List<ReviewEntry>
            //{
            //    new ReviewEntry { BookTitle = "君と僕のXYZ", Rate = 3, ReviewText = "ウゴウゴルーガ"}
            //};

            //var reviewList = new List<string>
            //{
            //    "ファイナル・スモトリ",
            //    "君と僕のXYZ",
            //    "不老不死になってしたい47つのこと"
            //};

            try
            {
                if (m_authenticated == true)
                {
                    var reviewList = await App.reviewTable.Select(x => x.BookTitle).ToListAsync();

                    ui_listview.ItemsSource = reviewList;
                }
                else
                {
                    await DisplayAlert("認証エラー", "認証がありません。", "OK");
                }
            }
            catch (MobileServiceInvalidOperationException msioe)
            {
                Debug.WriteLine(@"Invalid sync operation: {0}", msioe.Message);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"Sync error: {0}", ex.Message);
            }

            //var accepted = await this.DisplayAlert(
            //    "ダイアログのタイトル", "ダイアログの文字列", "はい", "いいえ");
            //if (accepted)
            //{
            //    ui_label.Text = "Accepted!";
            //}
        }

        async void OnLoginButtonClicked(object sender, EventArgs e)
        {
            if (App.Authenticator != null)
                m_authenticated = await App.Authenticator.Authenticate();

            //try
            //{
            //    // Change 'MobileService' to the name of your MobileServiceClient instance.
            //    // Sign-in using Facebook authentication.
            //    var token = new JObject();
            //    token.Add("access_token", "EAAZAIegqGGXoBAHi73mLovfqQrVERFkiJ6R3CcurZCPyFA2Km2jGTZAsppzc7pDdmK4ZBTIg7t2ZAtBMWZBDGvN6TYSvqeTA6tr9v6xGW41SSpr5zYY53gBUcoN6iaD3FcNGSdXg6U5Qn1xg7ZA6Tfe9hnDdKYU7XMZD");

            //    App.user = await App.client
            //        .LoginAsync(MobileServiceAuthenticationProvider.Facebook, token);
            //    ui_label.Text =
            //        string.Format("You are now signed in - {0}", App.user.UserId);
            //}
            //catch (InvalidOperationException)
            //{
            //    ui_label.Text = "You must log in. Login Required";
            //}
            //catch(Exception ex)
            //{
            //    var test = ex.Message;
            //}
        }

        async void OnNavigateNewPage(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new TestPage());
            await Navigation.PushAsync(new TestTabbedPage());
        }
    }
}
